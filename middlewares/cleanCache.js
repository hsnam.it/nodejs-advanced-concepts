const { clearCache } = require("../services/cache");
module.exports = async (req, res, next) => {
  await next();

  clearCache(req.user.id);

  // TODO: this middleware didn't work as desired.
  // exprectation: this will be run after endpoint handler finished executing
  console.log("Call in middleware");
};
