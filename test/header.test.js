const puppeteer = require("puppeteer");

let browser, page;
beforeEach(async () => {
  // Launch the browser
  browser = await puppeteer.launch({
    headless: false
  });

  // Open a new tab to work
  page = await browser.newPage();

  // navigate to the target url
  await page.goto("localhost:3000");
});

afterEach(async () => {
  await browser.close();
});

test("Logo has a correct text", async () => {
  // get text of an element in DOM
  const logoText = await page.$eval("a.brand-logo", el => el.innerHTML);

  expect(logoText).toEqual("Blogster");
});

test("Click the login button to start oauth flow", async () => {
  await page.click(".right a");
  const url = await page.url();

  // toMatch(regex)
  expect(url).toMatch(/accounts\.google\.com/);
});
