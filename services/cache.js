const mongoose = require("mongoose");
const redis = require("redis");
const util = require("util");

const exec = mongoose.Query.prototype.exec;
const redisUrl = "redis://127.0.0.1:6379";
const client = redis.createClient(redisUrl);
client.hget = util.promisify(client.hget);

mongoose.Query.prototype.cache = function(options = {}) {
  this.useCache = true;
  this.targetUser = options.targetUser || "";
  return this;
};

mongoose.Query.prototype.exec = async function() {
  if (!this.useCache) {
    return exec.apply(this, arguments);
  }

  const key = createRedisKeyString(
    this.getQuery(),
    this.mongooseCollection.name
  );
  const cacheValue = await client.hget(this.targetUser, key);
  if (cacheValue) {
    const doc = JSON.parse(cacheValue);

    // new this.model(doc): create mongo document object because data returned from redis is just a plain JSON object
    return Array.isArray(doc)
      ? doc.map(d => new this.model(d))
      : new this.model(doc);
  }

  // apply: default method of function
  // arguments: default object in function
  const result = await exec.apply(this, arguments);
  client.hset(this.targetUser, key, JSON.stringify(result));
  return result;
};

function createRedisKeyString(query, collection) {
  const key = Object.assign({}, { query }, { collection });
  // Must use JSON.stringify in order to the function JSON.parse works properly later
  return JSON.stringify(key);
}

module.exports = {
  clearCache: function(targetUser) {
    client.DEL(targetUser);
  }
};
